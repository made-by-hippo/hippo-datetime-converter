<?php

/*
=====================================================
=====================================================
 File: pi.datetime_convert.php
-----------------------------------------------------
 Purpose: Date Time Converter.  This class an 
 ExpressionEngine Date/Time and converts it to another 
 language for multi-lingual sites.
=====================================================
*/

$plugin_info = array(
						'pi_name'			=> 'Made By Hippo Language Date/Time Convert',
						'pi_version'		=> '1.0',
						'pi_author'			=> 'Made By Hippo',
						'pi_author_url'		=> 'http://www.madebyhippo.com/addon-shack/',
						'pi_description'	=> 'Allows you to convert your EE date/time into a different language',
						'pi_usage'			=> Datetime_convert::usage()
					);



class Datetime_convert {

 var $return_data;
    
// ----------------------------------------
//  Plugin Usage
// ----------------------------------------

// This function describes how the plugin is used.
//  Make sure and use output buffering

// Thanks to Peter Siska (http://www.designchuchi.ch) for a slight tweak to the date logic.

 function datetime_convert()
    {    
        header('Content-Type: text/html; charset=utf-8');
        
        global $TMPL;
        
        $language=$TMPL->fetch_param('language');
        $format = $TMPL->fetch_param('format');
        
   		$olddate = $TMPL->tagdata;
       
       	setlocale(LC_ALL, $language);
       	
       	$trans = get_html_translation_table(HTML_ENTITIES);
       	
        $newdate = strtr(strftime($format,$olddate), $trans);
		
		$this->return_data = $newdate;
    }


function usage()
{
ob_start(); 
?>
Simply encapsulate your ExpressionEngine Date/Time into the following Tags :

{exp:datetime_convert language="" format=""}{/exp:datetime_convert}

Passing the appropriate Language reference into the LANG="" parameter.

* fr_FR : French
* es_ES : Spanish
* nl_NL : Dutch

Depending on what Locales are installed on your server. You can check this by running 'locale -a' from a console window.

Passing the format that you would like the date/time returned in using the FORMAT="" parameter:

Default format is usually "%A %e %B %Y", however, review http://php.net/manual/en/function.strftime.php for full details on formatting
<?php
$buffer = ob_get_contents();
	
ob_end_clean(); 

return $buffer;
}
/* END */

}
?>